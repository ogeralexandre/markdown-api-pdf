var express = require('express');
var router = express.Router();
var fs = require('fs');
var zlib = require('zlib');
var AWS = require('aws-sdk');
var request = require('request');
var uuid = require('node-uuid');
var markdownpdf = require('markdown-pdf');
var scissors = require('scissors');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/*/*/*/*/:github', function(req, res, next) {

  var url = 'http://github.com/' + Object.keys(req.params)
    .map(function(value) { return req.params[value]; })
    .join('/')
    .replace('blob', 'raw');

  var file_uuid = uuid.v1();
  var localPath = './tmp/' + file_uuid;
  var file = fs.createWriteStream(localPath);

  // download markdown
  request
    .get(url)
    .on('error', function(err) {
      console.log(err);
    })
    .pipe(file);

  file.on('close', convertPDF);

  function convertPDF() {
    // convert it in pdf
    var markdown = fs.createWriteStream(localPath + '.pdf');
    fs.createReadStream(localPath)
    .pipe(markdownpdf())
    .pipe(markdown)

    // upload it to S3
    markdown.on('close', uploadS3);
  }

  function uploadS3() {
    AWS.config.loadFromPath('./config.json');
    var body = fs.createReadStream(localPath + '.pdf');
    var s3obj = new AWS.S3({params: {Bucket: 'alexandreogermk2pdf', Key: file_uuid + '.pdf', endpoint :'https://s3-us-west-1.amazonaws.com'}});
    s3obj.upload({Body: body}).
      on('httpUploadProgress', function(evt) { console.log(evt); }).
      send(function(err, data) {

        fs.unlinkSync(localPath);
        fs.unlinkSync(localPath + '.pdf');
        res.redirect(data.Location);
      });
  }
});

module.exports = router;
